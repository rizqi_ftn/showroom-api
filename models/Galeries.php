<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "galeries".
 *
 * @property int $galeri_id
 * @property string $nama_mobile
 * @property string $file_id untuk gambarnya
 * @property string $tipe
 * @property string $spesifikasi
 * @property int $created_by
 * @property string $created_date
 * @property int $updated_by
 * @property string $updated_date
 */
class Galeries extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'galeries';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['spesifikasi'], 'string'],
            [['created_by', 'updated_by'], 'integer'],
            [['created_date', 'updated_date'], 'safe'],
            [['nama_mobile', 'file_id', 'tipe'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'galeri_id' => 'Galeri ID',
            'nama_mobile' => 'Nama Mobile',
            'file_id' => 'File ID',
            'tipe' => 'Tipe',
            'spesifikasi' => 'Spesifikasi',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'updated_by' => 'Updated By',
            'updated_date' => 'Updated Date',
        ];
    }
}
